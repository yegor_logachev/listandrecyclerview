package com.logacfg.mvplecture.users;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.logacfg.mvplecture.R;
import com.logacfg.mvplecture.model.User;

import java.util.List;

/**
 * Created by Yegor on 8/5/17.
 */

public class UsersAdapter extends BaseAdapter {

    private List<User> users;
    private LayoutInflater inflater;

    public UsersAdapter(Context context, List<User> users) {
        this.users = users;
        inflater = LayoutInflater.from(context);
    }

    public UsersAdapter(Context context) {
        this(context, null);
    }

    public void setUsers(List<User> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    @Override
    public User getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_large, viewGroup, false);
            holder.nameTextView = view.findViewById(R.id.textViewName);
            holder.addressTextView = view.findViewById(R.id.textViewAddress);
            holder.phoneTextView = view.findViewById(R.id.textViewPhone);
            holder.button = view.findViewById(R.id.button);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        User item = getItem(i);
        holder.nameTextView.setText(item.getName());
        holder.addressTextView.setText(item.getAddress());
        holder.phoneTextView.setText(item.getPhone());
        holder.button.setOnClickListener(new MyBtmClickListener(i));
        return view;
    }

    @Override
    public int getCount() {
        return users == null ? 0 : users.size();
    }

    private class ViewHolder {
        TextView nameTextView;
        TextView addressTextView;
        TextView phoneTextView;
        Button button;
    }

    private class MyBtmClickListener implements View.OnClickListener {

        int position;

        public MyBtmClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            User item = getItem(position);
            Toast.makeText(view.getContext(), "On click: " + item.getPhone(), Toast.LENGTH_SHORT).show();
        }
    }
}
