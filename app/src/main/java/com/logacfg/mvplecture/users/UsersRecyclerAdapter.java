package com.logacfg.mvplecture.users;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.logacfg.mvplecture.R;
import com.logacfg.mvplecture.model.User;

import java.util.List;

/**
 * Created by Yegor on 8/5/17.
 */

public class UsersRecyclerAdapter extends RecyclerView.Adapter<UsersRecyclerAdapter.UsersViewHolder> {

    private List<User> users;
    private LayoutInflater inflater;

    public UsersRecyclerAdapter(Context context) {
        inflater = LayoutInflater.from(context);
    }

    public void setUsers(List<User> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    @Override
    public UsersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_large, parent, false);
        return new UsersViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UsersViewHolder holder, int position) {
        User user = users.get(position);
        holder.nameTextView.setText(user.getName());
        holder.addressTextView.setText(user.getAddress());
        holder.phoneTextView.setText(user.getPhone());
    }

    @Override
    public int getItemCount() {
        return users != null ? users.size() : 0;
    }

    public void addUserAsFirst(User user) {
        users.add(0, user);
        notifyItemInserted(0);
    }

    class UsersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView nameTextView;
        private TextView addressTextView;
        private TextView phoneTextView;
        private Button btn;

        UsersViewHolder(final View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.textViewName);
            phoneTextView = itemView.findViewById(R.id.textViewPhone);
            addressTextView = itemView.findViewById(R.id.textViewAddress);
            btn = itemView.findViewById(R.id.button);
            btn.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            User user = users.get(position);
            String text = view.getId() == R.id.button ? "ON_BUTTON_CLICK: " + user.getName() : "ON_ITEM_CLICK: " + user.getAddress();
            Toast.makeText(view.getContext(), text, Toast.LENGTH_SHORT).show();
        }
    }
}
