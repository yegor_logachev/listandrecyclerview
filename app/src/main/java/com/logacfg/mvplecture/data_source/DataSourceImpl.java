package com.logacfg.mvplecture.data_source;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import com.logacfg.mvplecture.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yegor on 7/29/17.
 */

public class DataSourceImpl implements DataSource {

    private SQLiteOpenHelper helper;

    public DataSourceImpl(Context context) {
        helper = new DatabaseHelper(context);
    }

    @Override
    public void insertUsers(List<User> users) {
        SQLiteDatabase db = helper.getWritableDatabase();
        for (User user : users) {
            db.insert(User.TABLE_NAME, null, user.getContentValues());
        }
        db.close();
    }

    @Override
    public long insertUser(User user) {
        SQLiteDatabase db = helper.getWritableDatabase();
        long id = db.insert(User.TABLE_NAME, null, user.getContentValues());
        db.close();
        return id;
    }

    @Override
    public List<User> loadUsers() {
        List<User> users = null;
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(User.TABLE_NAME, User.COLUMNS, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            users = new ArrayList<>(cursor.getCount());
            cursor.moveToFirst();
            do {
                users.add(new User(cursor));
            } while (cursor.moveToNext());
            cursor.close();
        }
        return users;
    }

    @Override
    @Nullable
    public User getUserById(long id) {
        SQLiteDatabase db = helper.getReadableDatabase();
        User user = null;
        Cursor cursor = db.query(User.TABLE_NAME, User.COLUMNS, User.ID + " = ?", new String[]{String.valueOf(id)}, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            user = new User(cursor);
            cursor.close();
        }
        return user;
    }
}
